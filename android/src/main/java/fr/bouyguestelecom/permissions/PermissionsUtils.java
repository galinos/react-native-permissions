package fr.bouyguestelecom.permissions;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

/**
 * Created by lmenness on 21/03/2017.
 */

public class PermissionsUtils {
    public static final int PERMISSION_REQUEST_CAMERA = 23;
    public static final int PERMISSION_REQUEST_ACCESS_LOCATION = 21;

    public static Boolean hasCoarseLocationPermission(Context context) {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    public static Boolean hasCameraPermission(Context context) {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
    }
}
