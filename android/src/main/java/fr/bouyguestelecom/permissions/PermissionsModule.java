package fr.bouyguestelecom.permissions;

import android.Manifest;
import android.content.DialogInterface;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by lmenness on 21/03/2017.
 */

public class PermissionsModule extends ReactContextBaseJavaModule {
    private static ReactApplicationContext _reactContext;

    private static Promise _promise;

    public PermissionsModule(ReactApplicationContext reactContext) {
        super(reactContext);
        _reactContext = reactContext;

    }

    @Override
    public String getName() {
        return "Permissions";
    }

    @ReactMethod
    public void hasCameraPermission(Promise promise) {
        if (PermissionsUtils.hasCameraPermission(_reactContext)) {
            promise.resolve(null);
        } else {
            promise.reject("");
        }
    }

    @ReactMethod
    public void requestCameraPermission(Promise promise) {
        if (!PermissionsUtils.hasCameraPermission(_reactContext)) {
            _promise = promise;
            ActivityCompat.requestPermissions(_reactContext.getCurrentActivity(), new String[]{Manifest.permission.CAMERA}, PermissionsUtils.PERMISSION_REQUEST_CAMERA);
        }
        else {
            promise.resolve(null);
        }
    }

    @ReactMethod
    public void hasLocationPermission(Promise promise) {
        if (PermissionsUtils.hasCoarseLocationPermission(_reactContext)) {
            promise.resolve(null);
        } else {
            promise.reject("");
        }
    }

    @ReactMethod
    public void requestLocationPermission(Promise promise) {
        Log.i("DEBUG", "requestLocationPermission : " + PermissionsUtils.hasCoarseLocationPermission(_reactContext));

        if (!PermissionsUtils.hasCoarseLocationPermission(_reactContext)) {
            _promise = promise;
            ActivityCompat.requestPermissions(_reactContext.getCurrentActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PermissionsUtils.PERMISSION_REQUEST_ACCESS_LOCATION);
        }
        else {
            promise.resolve(null);
        }
    }

    public static void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.i("DEBUG", "onRequestPermissionsResult requestCode : " + requestCode);


        switch (requestCode) {
            case PermissionsUtils.PERMISSION_REQUEST_CAMERA: {
                if (!PermissionsUtils.hasCameraPermission(_reactContext)) {
                    openDialog();
                } else {
                    if(_promise != null) {
                        _promise.resolve(null);
                        _promise = null;
                    }
                }
                return;
            }
            case PermissionsUtils.PERMISSION_REQUEST_ACCESS_LOCATION: {
                if (!PermissionsUtils.hasCoarseLocationPermission(_reactContext)) {
                    openDialog();
                } else {
                    if(_promise != null) {
                        _promise.resolve(null);
                        _promise = null;
                    }
                }
                return;
            }
        }
    }

    private static void openDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(_reactContext.getCurrentActivity());
        builder.setTitle(_reactContext.getResources().getString(fr.bouyguestelecom.permissions.R.string.permission_m_title));
        builder.setMessage(_reactContext.getResources().getString(fr.bouyguestelecom.permissions.R.string.permission_general_error)).setCancelable(true);
        builder.setPositiveButton(_reactContext.getResources().getString(fr.bouyguestelecom.permissions.R.string.permission_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1) {

            }
        });

        android.app.AlertDialog alert = builder.create();
        alert.show();
    }
}
